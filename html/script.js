/************************

CONSTANTS

*************************/

// MQ Colors
const COLOR_MQ_RED = "#A6192E";
const COLOR_MQ_DEEP_RED = "#76232F";
const COLOR_MQ_BRIGHT_RED = "#D6001C";
const COLOR_MQ_MAGENTA = "#D6001C";
const COLOR_MQ_PURPLE = "#80225F";
const COLOR_MQ_CHARCOAL = "#373A36";
const COLOR_MQ_SAND = "#D6D2C4";
const COLOR_FACULTY_FA = "#AAA77D";
const COLOR_FACULTY_FBE = "#0006FBA";
const COLOR_FACULTY_FHS = "#0097A0";
const COLOR_FACULTY_FSE = "#00AA4F";
const COLOR_FACULTY_FSHMS = "#A2AAAD";

// D3js force config
const NODE_CHARGE = -1400;
const NODE_RADIUS = 30;
const LINK_DIST = 200;

// Node and link styling
const NODE_DEPARTMENT_FILL = COLOR_MQ_SAND;
const NODE_UNIT_FILL = COLOR_MQ_SAND;
const NODE_UNIT_STROKE = COLOR_MQ_BRIGHT_RED;
const NODE_UNIT_STROKE_WIDTH = 0;
const NODE_UNIT_TEXT_COLOR = COLOR_MQ_CHARCOAL;
const NODE_UNIT_TEXT_SIZE = 8;

const NODE_MAJOR_FILL = COLOR_MQ_DEEP_RED;
const NODE_MAJOR_STROKE = COLOR_MQ_BRIGHT_RED;
const NODE_MAJOR_STROKE_WIDTH = 0;
const NODE_MAJOR_TEXT_COLOR = COLOR_MQ_SAND;
const NODE_MAJOR_TEXT_SIZE = 8;

const NODE_FOCUSED_FILL = COLOR_MQ_SAND;
const NODE_FOCUSED_STROKE = COLOR_MQ_BRIGHT_RED;
const NODE_FOCUSED_STROKE_WIDTH = 2;

const LINK_STROKE_WIDTH = 4;
const LINK_UNIT_OF_STROKE = COLOR_MQ_RED;
const LINK_UNIT_OF_STROKE_WIDTH = LINK_STROKE_WIDTH;
const LINK_UNIT_OF_OPTIONAL_STROKE = COLOR_MQ_SAND;
const LINK_UNIT_OF_OPTIONAL_STROKE_WIDTH = LINK_STROKE_WIDTH;


// Zoom/Pan config
const MIN_ZOOM = 0.1;
const MAX_ZOOM = 5;
const MAX_STROKE = LINK_STROKE_WIDTH * 2;
const MAX_BASE_NODE_SIZE = 100;
const MAX_TEXT_SIZE = 50;
const TEXT_CENTER = true;


/************************

FUNCTIONS

*************************/

var force;
var focusedNode;

function contextMenu() {
    var height,
        width,
        margin = 0.1, // fraction of width
        items = [],
        rescale = false,
        style = {
            'rect': {
                'mouseout': {
                    'fill': 'rgb(244,244,244)',
                    'stroke': 'white',
                    'stroke-width': '1px'
                },
                'mouseover': {
                    'fill': 'rgb(200,200,200)'
                }
            },
            'text': {
                'fill': 'steelblue',
                'font-size': '13'
            }
        };

    function menu(x, y, obj) {
        d3.select('.context-menu').remove();
        scaleItems();

        // Draw the menu
        d3.select('svg')
            .append('g').attr('class', 'context-menu')
            .selectAll('tmp')
            .data(items).enter()
            .append('g').attr('class', 'menu-entry')
            .style({
                'cursor': 'pointer'
            })
            .on('mouseover', function () {
                d3.select(this).select('rect').style(style.rect.mouseover)
            })
            .on('mouseout', function () {
                d3.select(this).select('rect').style(style.rect.mouseout)
            });

        d3.selectAll('.menu-entry')
            .append('rect')
            .attr('x', x)
            .attr('y', function (d, i) {
                return y + (i * height);
            })
            .attr('width', width)
            .attr('height', height)
            .style(style.rect.mouseout);

        d3.selectAll('.menu-entry')
            .append('text')
            .text(function (d) {
                return d;
            })
            .attr('x', x)
            .attr('y', function (d, i) {
                return y + (i * height);
            })
            .attr('dy', height - margin / 2)
            .attr('dx', margin)
            .style(style.text);

        d3.selectAll('.menu-entry')
            .on('click', function (d) {
                handleContextMenuAction(d, obj);
            });

        // Other interactions
        d3.select('body')
            .on('click', function () {
                force.start();
                d3.select('.context-menu').remove();
            });

    }

    menu.items = function (e) {
        if (!arguments.length) return items;
        for (i in arguments) items.push(arguments[i]);
        rescale = true;
        return menu;
    }

    // Automatically set width, height, and margin;
    function scaleItems() {
        if (rescale) {
            d3.select('svg').selectAll('tmp')
                .data(items).enter()
                .append('text')
                .text(function (d) {
                    return d;
                })
                .style(style.text)
                .attr('x', -1000)
                .attr('y', -1000)
                .attr('class', 'tmp');
            var z = d3.selectAll('.tmp')[0]
                .map(function (x) {
                    return x.getBBox();
                });
            width = d3.max(z.map(function (x) {
                return x.width;
            }));
            margin = margin * width;
            width = width + 2 * margin;
            height = d3.max(z.map(function (x) {
                return x.height + margin / 2;
            }));

            // cleanup
            d3.selectAll('.tmp').remove();
            rescale = false;
        }
    }

    return menu;
}

// Function to handle actions requested from the right click menu
function handleContextMenuAction(d, obj) {
    if (d == 'See other degrees with this unit') {
        focusedNode = obj.title;
        requestUnits([obj.code])


    } else if (d == 'Check out the handbook page') {
        var win = window.open('http://www.google.com/search?q=' + encodeURI("mq handbook " + obj.title), '_blank');
        if (win) {
            //Browser has allowed it to be opened
            win.focus();
        } else {
            //Browser has blocked it
            alert('Please allow popups for this website');
        }
    }
}

//the helper function provided by neo4j documents
function idIndex(a, id) {
    for (var i = 0; i < a.length; i++) {
        if (a[i].id == id) return i;
    }
    return null;
}

function requestUnits(units) {
    units_string = units[0];
    for (var i = 1; i < units.length; i++) {
        units_string = units_string + "+" + units[i];
    }

    var request = $.get($(location).attr('href') + "Unit/" + units_string, function(data) {
        var nodes = [],
                links = [];


        data.results.forEach(function (result) {
            result.data.forEach(function (row) {
                row.graph.nodes.forEach(function (n) {
                    if (idIndex(nodes, n.id) == null) {
                        nodes.push({
                            id: n.id,
                            label: n.labels[0],
                            title: n.properties.name,
                            code: n.properties.code
                        });
                    }
                });
                links = links.concat(row.graph.relationships.map(function (r) {
                    // the neo4j documents has an error : replace start with source and end with target
                    return {
                        source: idIndex(nodes, r.startNode),
                        target: idIndex(nodes, r.endNode),
                        type: r.type
                    };
                }));
            });
        });


        var graph = {
            nodes: nodes,
            links: links
        };
        drawGraph(graph);
    });
}

function requestMajors(majors) {

    majors_string = majors[0];
    for (var i = 1; i < majors.length; i++) {
        majors_string = majors_string + "+" + majors[i];
    }

    var request = $.get($(location).attr('href') + "Major/" + majors_string, function(data) {
        var nodes = [],
                links = [];


        data.results.forEach(function (result) {
            result.data.forEach(function (row) {
                row.graph.nodes.forEach(function (n) {
                    if (idIndex(nodes, n.id) == null) {
                        nodes.push({
                            id: n.id,
                            label: n.labels[0],
                            title: n.properties.name,
                            code: n.properties.code
                        });
                    }
                });
                links = links.concat(row.graph.relationships.map(function (r) {
                    // the neo4j documents has an error : replace start with source and end with target
                    return {
                        source: idIndex(nodes, r.startNode),
                        target: idIndex(nodes, r.endNode),
                        type: r.type
                    };
                }));
            });
        });


        var graph = {
            nodes: nodes,
            links: links
        };
        drawGraph(graph);
    });
}

// Draws grpah when given graph object
function drawGraph(graph) {
    var width = $("#graph").width();
    var height = $("#graph").height();

    var size = d3.scale.pow().exponent(1)
        .domain([1, 100])
        .range([8, 24]);

    var menu = contextMenu().items('See other degrees with this unit', 'Check out the handbook page');

    force = d3.layout.force()
        .charge(NODE_CHARGE)
        .linkDistance(LINK_DIST)
        .size([width, height]);

    $("#graph").html("")

    var svg = d3.select("#graph").append("svg")
        .attr("width", "100%")
        .attr("height", "100%")
        .attr("pointer-events", "all");

    var zoom = d3.behavior.zoom().scaleExtent([MIN_ZOOM, MAX_ZOOM])
    var g = svg.append("g");


    force.nodes(graph.nodes).links(graph.links).start();

    var link = g.selectAll(".link")
        .data(graph.links).enter()
        .append("line").attr("class", "link")
        .attr("class", function (d) {
            return "link_" + d.type;
        });

    var node = g.selectAll(".node")
        .data(graph.nodes)
        .enter()
        .append('g')
        .on("contextmenu", function (obj) {
            d3.event.preventDefault();
            force.stop();
            menu(d3.mouse(svg.node())[0], d3.mouse(svg.node())[1], obj)
        })
        .classed('node', true)
        .call(force.drag);

    node.on("dblclick.zoom", function (d) {
        d3.event.stopPropagation();
        var dcx = (width / 2 - d.x * zoom.scale());
        var dcy = (height / 2 - d.y * zoom.scale());
        zoom.translate([dcx, dcy]);
        g.attr("transform", "translate(" + dcx + "," + dcy + ")scale(" + zoom.scale() + ")");
    });


    var circle = node.append("circle")
        .attr("class", function (d) {
            return "node_" + d.label
        })
        .attr("r", NODE_RADIUS);

    var text = node.append("text")
        .attr("dx", -20)
        .attr("dy", ".35em")
        .classed("unselectable", true)
        .style("font-family", '"Trebuchet MS", Helvetica, sans-serif')
        .style("font-size", NODE_UNIT_TEXT_SIZE + "pt")
        .text(function (d) {
            if (d.label == "Unit") {
                return d.code;
            } else if (d.label == "Major") {
                return d.code;
            } else {
                return "";
            }
        })
        .attr("fill", function (d) {
            if (d.label == "Unit") {
                return NODE_UNIT_TEXT_COLOR;
            } else if (d.label == "Major") {
                return NODE_MAJOR_TEXT_COLOR;
            } else {
                return "black"
            }
        });

    // html title attribute
    node.append("title")
        .text(function (d) {
            return d.title;
        })

    // color
    g.selectAll(".node_Major")
        .attr("fill", NODE_MAJOR_FILL);

    g.selectAll(".node_Unit")
        .attr("fill", function (d) {
            if (d.title == focusedNode) {
                return NODE_FOCUSED_FILL
            } else {
                return NODE_UNIT_FILL
            }
        })
        .attr("stroke", function (d) {
            if (d.title == focusedNode) {
                return NODE_FOCUSED_STROKE
            } else {
                return NODE_UNIT_STROKE
            }
        })
        .attr("stroke-width", function (d) {
            if (d.title == focusedNode) {
                return NODE_FOCUSED_STROKE_WIDTH
            } else {
                return NODE_UNIT_STROKE_WIDTH
            }
        });

    g.selectAll(".link_UNIT_OF")
        .attr("stroke", LINK_UNIT_OF_STROKE)
        .attr("stroke-width", LINK_UNIT_OF_STROKE_WIDTH);

    g.selectAll(".link_UNIT_OF_optional_list")
        .attr("stroke", LINK_UNIT_OF_OPTIONAL_STROKE)
        .attr("stroke-width", LINK_UNIT_OF_OPTIONAL_STROKE_WIDTH);

    zoom.on("zoom", function () {
        g.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    });

    svg.call(zoom);

    // force feed algo ticks
    force.on("tick", function () {
        link.attr("x1", function (d) {
                return d.source.x;
            })
            .attr("y1", function (d) {
                return d.source.y;
            })
            .attr("x2", function (d) {
                return d.target.x;
            })
            .attr("y2", function (d) {
                return d.target.y;
            });

        node.attr("transform", function (d) {
            return 'translate(' + [d.x, d.y] + ')';
        })

        /*node.attr("cx", function(d) { return d.x; })
                .attr("cy", function(d) { return d.y; });*/

    });
}

function fillLegend() {
    // Unit node
    var svg = d3.select("#legend-node-unit").append("svg").attr("width", "60px").attr("height", "60px");
    svg.append("circle").attr("r", NODE_RADIUS).attr("fill", NODE_UNIT_FILL).attr("stroke", NODE_UNIT_STROKE).attr("stroke-width", NODE_UNIT_STROKE_WIDTH).attr("cx", 30).attr("cy", 30);

    // Major node
    var svg = d3.select("#legend-node-major").append("svg").attr("width", "60px").attr("height", "60px");
    svg.append("circle").attr("r", NODE_RADIUS).attr("fill", NODE_MAJOR_FILL).attr("stroke", NODE_MAJOR_STROKE).attr("stroke-width", NODE_MAJOR_STROKE_WIDTH).attr("cx", 30).attr("cy", 30);

    // Required Link
    var svg = d3.select("#legend-link-required").append("svg").attr("width", "60px").attr("height", "60px");
    svg.append("line").attr("x1", 0).attr("y1", 30).attr("x2", 60).attr("y2", 30).attr("stroke", LINK_UNIT_OF_STROKE).attr("stroke-width", LINK_UNIT_OF_STROKE_WIDTH);

    // Required Link
    var svg = d3.select("#legend-link-optional").append("svg").attr("width", "60px").attr("height", "60px");
    svg.append("line").attr("x1", 0).attr("y1", 30).attr("x2", 60).attr("y2", 30).attr("stroke", LINK_UNIT_OF_OPTIONAL_STROKE).attr("stroke-width", LINK_UNIT_OF_OPTIONAL_STROKE_WIDTH);
}

/************************

JQUERY

*************************/


// Submit action for config form
$("#config-submit").on("click", function (e) {
    e.preventDefault();
    majors = []
    $("#major").val().forEach(function (major) {
        
    });
    requestGraph(query);
});

// Filter multiple select with search box as you type
$(".search-filter").keyup(function () {
    $(this).next().children().each(function () {
        if ($(this).text().toLowerCase().includes($(this).parent().prev().val())) {
            $(this).show()
        } else {
            $(this).hide()
        }
    })
});

$(document).ready(function () {
    // The default query
    
    requestMajors(["DAS18V1", "SOT18V1"]);

    fillLegend()
});
