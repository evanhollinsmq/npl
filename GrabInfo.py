import requests


url = "http://api.prod.handbook.mq.edu.au/"
api_key = "9f9ef28dea630ae6311cc730207b2b59"
#request_string = "Units/JSON/2017/"

def grabInfo(request_string):
    if request_string[0] == "/":
        request_string = request_string[1:]
    if request_string[-1] != "/":
        request_string += "/"
    return requests.get(url+request_string+api_key).json()
