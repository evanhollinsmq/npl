from neo4j.v1 import GraphDatabase
from GrabInfo import grabInfo
from nodes import degreeCommand, unitCommand

year = '2018'
uri = "bolt://localhost:7687"
driver = GraphDatabase.driver(uri, auth=("client", "password"))
degrees = grabInfo("Degrees/JSON/{0}/".format(year))
units = grabInfo("Units/JSON/{0}/".format(year)) # just undergraduate units
majors = grabInfo("Majors/JSON/{0}".format(year))

with driver.session() as session:
    with session.begin_transaction() as tx:


        def connectUnits(requirements, overallMatch):
            for lvlKey in requirements:
                for i in requirements[lvlKey]:
                    print("hello")
                    unitItem = i
                    reqGp = unitItem["reqGp"]
                    reqs = reqGp["reqs"]
                    # if reqs.length =1 --> required single unit or pick any comp***
                    if len(reqs) == 1:
                        if reqs[0]["unitNumber"] != "":
                            # add this unit connection as a required single unit
                            # print("required single unit")
                            prefix = reqs[0]["unitPrefix"]
                            number = reqs[0]["unitNumber"]
                            # print(prefix+number)
                            unitMatch = "MATCH (u:Unit) WHERE u.code = '{0}' ".format(prefix + number)
                            command = overallMatch + unitMatch + "CREATE (u)-[:UNIT_OF]->(m)"
                            print(command)
                            tx.run(command)

                        # else:
                        # this is any unit in COMP*** (corner case implement later)
                        # print("pick any units of: ", reqs[0]["unitPrefix"])
                        # not implementing yet

                    else:
                        # print("pick some of these units")
                        for j in reqs:
                            unit = j
                            prefix = unit["unitPrefix"]
                            number = unit["unitNumber"]
                            # print(prefix+number)
                            unitMatch = "MATCH (u:Unit) WHERE u.code = '{0}' ".format(prefix + number)
                            command = overallMatch + unitMatch + "CREATE (u)-[:UNIT_OF_optional_list]->(m)"
                            print(command)
                            tx.run(command)


        tx.run("MATCH p=()-[]->() DELETE p")

        ### create nodes ###
        for record in degrees:
            tx.run(degreeCommand(record['Id'], year))

        for record in units:
            tx.run(unitCommand(record['Id'], year))

        departmentID = 0
        departmentNames = []
        facultyID = 0
        facultyNames = []
        for record in majors:
            moreInfo = grabInfo("Major/JSON/{0}/{1}".format(record['Code'], year))

            tx.run("CREATE (M{0}:Major {{ code:'{1}' ,name:\"{2}\", cp:{3}, version:{4} }})".format(record['Id'], record['Code'], record['Name'], moreInfo['TotalCP'], record['Version']))

            # create faculty nodes
            if (record['Faculty'] not in facultyNames):
                facultyNames.append(record["Faculty"])
                tx.run("CREATE (F{0}:Faculty {{name:\"{1}\"}})".format(facultyID, record['Faculty']))
                facultyID = facultyID + 1

            # crearte department nodes
            if (record['Department'] not in departmentNames):
                departmentNames.append(record['Department'])
                tx.run("CREATE (P{0}:Department {{name:\"{1}\"}})".format(departmentID, record['Department']))
                departmentID = departmentID + 1


            ## connections ##
            majorMatch = "MATCH (m:Major) WHERE m.code = '{0}' ".format(record['Code'])
            degreesMore = moreInfo['Degrees']
            ## connect major to degree ##
            for degree in degreesMore:
                degreeMatch = "MATCH (d:Degree) WHERE d.code = '{0}' ".format(degree['DegreeAbbrev'])
                command = majorMatch + degreeMatch + "CREATE (d)-[:MAJORING_IN]->(m)"
                tx.run(command)

            ## connect major to department ##
            departmentMatch = "MATCH (p:Department) WHERE p.name = \"{0}\" ".format(record['Department'])
            tx.run(majorMatch + departmentMatch + "CREATE (m)-[:BELONGS_TO]->(p)")


            # connect major to unit
            connectUnits(moreInfo["Requirements"], majorMatch)


        ### create connections ###
        for record in units:
            unitMatch = "MATCH (u:Unit) WHERE u.code = \"{0}\" ".format(record['Code'])
            if record['Department']: # because some units don't have departments
                departmentMatch = "MATCH (p:Department) WHERE p.name = \"{0}\" ".format(record['Department'])
                command = unitMatch + departmentMatch + "CREATE (u)-[:PART_OF]->(p)"
                tx.run(command)


            # if record['Faculty']:
            #     facultyMatch = "MATCH (f:Faculty) WHERE f.name = \"{0}\" ".format(record['Faculty'])
            #     command = unitMatch + facultyMatch + "CREATE (u)-[:PART_OF_FACULTY]->(f)"
            #     tx.run(command)

        for record in degrees:
            degreeMatch = "MATCH (d:Degree) WHERE d.code = '{0}' ".format(record['Code'])

            # connect degree to faculty
            if(record['Faculty']):
                facultyMatch = "MATCH (f:Faculty) WHERE f.name = '{0}' ".format(record['Faculty'])
                tx.run(degreeMatch + facultyMatch + "CREATE (d)-[:IN_FACULTY]->(f)")

            # connect degree to units
            moreInfo = grabInfo("Degree/JSON/{0}/{1}".format(record['Code'], year))
            overallMatch = "MATCH (m:Degree) WHERE m.code = '{0}' ".format(record['Code'])
            for program in moreInfo['Program']:
                if('SpecificReqs' in program):
                    connectUnits(program['SpecificReqs'], overallMatch)
