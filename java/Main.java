public class Main{

  public static void main(String[] args){

    SmallExample example = new SmallExample("bolt://localhost:7687", "neo4j", "password");
    example.addPerson("Ada");
    example.addPerson("Alice");
    example.addPerson("Bob");
    example.printPeople("A");
    example.close();

  }
}
