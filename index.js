var request = require('request');
var express = require('express');
var app = express();

var unitTest = RegExp("[a-zA-z]{3,4}[0-9]{3}");

app.use(express.static('public'));
app.get('/', function (req, res) {
   res.sendFile( __dirname + "/html/" + "index.html" );
})
app.get('/script.js', function (req, res) {
   res.sendFile( __dirname + "/html/" + "script.js" );
})
app.get('/Unit/:unit', function (req, res) {
	if(unitTest.test(req.params.unit)) {
		var stmnt = 'MATCH (a:Unit)-[r1]->(b:Major) WHERE a.code="' + req.params.unit.toUpperCase() + '" AND TYPE(r1) IN["UNIT_OF", "UNIT_OF_optional_list"] \n MATCH p=(:Unit)-[r2]->(c:Major) WHERE c.code = b.code AND TYPE(r2) IN ["UNIT_OF", "UNIT_OF_optional_list"] RETURN p';

		var options = {  
		    url: 'http://client:password@localhost:7474/db/data/transaction/commit',
		    method: 'POST',
		    headers: {
		        'Accept': 'application/json',
		        'Accept-Charset': 'utf-8',
		        'Username': 'client',
		        'Password': 'password'
		    },
		    body: JSON.stringify({
				'statements' : [ {
					'statement' : stmnt,
					'resultDataContents': ['graph', 'row']
				} ]
			})
		};

		request(options, function(err, response, body) {  
		    res.send(JSON.parse(body));
		});


	} else {
		res.status(500).send();
	}
});

app.get('/Major/:majors', function (req, res) {

	bodyJSON = {
			statements : []
		}


	var majors = req.params.majors.split("+")

	for (var i = 0; i < majors.length; i++) {
		bodyJSON.statements.push(
				{
					statement : "MATCH p=(a:Unit)-[r1]->(b:Major) WHERE b.code='" + majors[i].replace(/["']/g, "") + "' AND TYPE(r1) IN['UNIT_OF', 'UNIT_OF_optional_list'] RETURN p",
					resultDataContents: ['graph', 'row']
				}
			);
	}

	var options = {  
	    url: 'http://client:password@localhost:7474/db/data/transaction/commit',
	    method: 'POST',
	    headers: {
	        'Accept': 'application/json',
	        'Accept-Charset': 'utf-8',
	        'Username': 'client',
	        'Password': 'password'
	    },
	    body: JSON.stringify(bodyJSON)
	};

	request(options, function(err, response, body) {  
	    res.send(JSON.parse(body));
	});

})

var server = app.listen(8080, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)

})
