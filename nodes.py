from GrabInfo import grabInfo

def degreeCommand(id, year):

    """
        Create command to be run to create unit node
        Fields include:
            id
            code
            name
            version
            type
            honours

    """

    d = grabInfo("Degree/JSON/{0}/{1}".format(id, year))
    command = "CREATE (d:Degree {{id:{0}, code:'{1}', name:\"{2}\", version:{3}, type:\"{4}\", honours:{5} }})".format(
        d["Id"], d["Abbreviation"], d["Name"], d["Version"], d["Type"], d["HonoursAvailable"])

    return command

def unitCommand(id, year):
    """
        Create command to be run to create unit node
        Fields include:
            id
            code
            name
            version
            credit points
            assessed as
            description
    """

    u = grabInfo("Unit/JSON/{0}/{1}".format(id, year))
    description = u['Description'].replace("\"", "\\\"")
    command = "CREATE (u:Unit {{id:{0}, code:'{1}', name:\"{2}\", version:{3}, cp:{4}, assessed:\"{5}\", desc:\"{6}\" }})".format(
        u['Id'], u['Code'], u['Name'], u["Version"], u['CreditPoints'], u['AssessedAs'], description,)

    return command

